/**********************
* OmniPhaser Development
* http://omniphaser.wc.lt
* Copyright (C) 2015
* ALL CODE LICENSES APPLY
**********************/

var keysPressed = [];
void keyPressed() {
	keysPressed[keyCode] = true;
};
void keyReleased() {
	keysPressed[keyCode] = false;
};
// Global variables
var page = 'boot';
var credits;
var counter = 1;
var lastMenuTime = 0;
boolean touch = false;
// Setup the Processing Canvas
void setup(){
  size( 340, 567 );
  strokeWeight( 1 );
  frameRate( 10 );
  background(100);
  loadImages();
}
void loadImages()
{
	credits = loadImage('credits.png');
}
// Main draw loop
void draw(){
  if (page == 'home')
  {
	  background(100);
	  stroke(200,200,200);
	  fill(200,200,200);  
	  Ginput();
	  phone();  
  }
  if (page == 'boot')
  {
	  background(100);
	  stroke(200,200,200);
	  fill(200,200,200);  
	  Ginput();
	  boot();  
  }
}
// Set circle's next destination [off]
void mouseMoved(){
  
}	

void Ginput()
{
   touch = false;
  if (mousePressed)
  {
	  touch = true;
  }
  
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}
function deleteCookie(name) {

    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
void phone()
{
	fill(220);
	int border = 10;
	//frame
	rect(border, border, width-2*border, height-2*border);
	//button
	if (mouseX > width/2-20 && mouseX < (width-width/2+20)&& mouseY > height-4*border && mouseY < height-2*border+15)
	{
		fill(100);
		rect(width/2-20, height-3*border, 40, 10);
		if (mousePressed)
		{
			home();
			fill(170);
			rect(width/2-20, height-3*border, 40, 10);		
		}
	}
	else
	{
		fill(30);
		rect(width/2-20, height-3*border, 40, 10);
	}
	apps();
}
void boot()
{
	stroke(20,20,20);
	fill(20,20,20);
	
	int border = 10;
	//frame
	rect(border, border, width-2*border, height-2*border);
	if (frameCount < 10+lastMenuTime)
	{
		image(credits, border, border);
	}
	else
	{
		tint(255, 255-counter*10);
		image(credits, border, border);
		//background(255-counter);
		counter+=1;
		if (counter  >= 256/10)
		{
			page = 'home';
		}
	}
	//button
	if (mouseX > width/2-20 && mouseX < (width-width/2+20)&& mouseY > height-4*border && mouseY < height-2*border+15)
	{
		fill(100);
		rect(width/2-20, height-3*border, 40, 10);
		if (mousePressed)
		{
			fill(170);
			rect(width/2-20, height-3*border, 40, 10);		
		}
	}
	else
	{
		fill(70);
		rect(width/2-20, height-3*border, 40, 10);
	}
	
}
void home()
{
	page = 'home';
}
void apps()
{
	fill(150);
	//AppGrid: appsize
	var appleft = 30;
	var apptop = 30;
	var appsize = 40;
	rect(appleft, apptop, appsize, appsize);rect(appleft+appsize*2, apptop, appsize, appsize);rect(appleft+appsize*4, apptop, appsize, appsize);rect(appleft+appsize*6, apptop, appsize, appsize);
	rect(appleft, apptop+appsize*2, appsize, appsize);rect(appleft+appsize*2, apptop+appsize*2, appsize, appsize);rect(appleft+appsize*4, apptop+appsize*2, appsize, appsize);rect(appleft+appsize*6, apptop+appsize*2, appsize, appsize);
	textSize(8);
	fill(0);
	text('Tech Support',appleft,apptop+appsize+8);
	text('Pi',appleft+appsize*2,apptop+appsize+8);
	text('IGEC',appleft+appsize*4,apptop+appsize+8);
	text('My Phone',appleft+appsize*6,apptop+appsize+8);
	if (mouseX > appleft && mouseX < appleft+appsize && mouseY>apptop && mouseY < apptop+appsize && mousePressed)
	{
		location.href='apps/techsupport/index.html';
	}
	if (mouseX > appleft+appsize*2 && mouseX < appleft+appsize*2+appsize && mouseY>apptop && mouseY < apptop+appsize && mousePressed)
	{
		location.href='apps/pi/index.html';
	}
	if (mouseX > appleft+appsize*4 && mouseX < appleft+appsize*4+appsize && mouseY>apptop && mouseY < apptop+appsize && mousePressed)
	{
		location.href='apps/IGEC/index.html';
	}
	if (mouseX > appleft+appsize*6 && mouseX < appleft+appsize*6+appsize && mouseY>apptop && mouseY < apptop+appsize && mousePressed)
	{
		location.href='apps/myphone/index.html';
	}
}