<?php
SESSION_START();
if ($_SESSION['l'] != '1')
{
	header('Location: auth.php');
}
?>
<!DOCTYPE HTML>
<html>

	<head>

		<title>PiePhone</title>

        <script src="processing.js"></script>

		<style>

		canvas {

    		

    		-webkit-touch-callout: none;

       		-webkit-user-select: none;

        	-khtml-user-select: none;

        	-moz-user-select: none;

        	-ms-user-select: none;

        	user-select: none;

        	outline: none;

        	border-radius: 25px;

			padding-top: 10px;

			padding-bottom: 10px;

		}

		</style>

	<head>

    <body>

        <div align="center"><canvas data-processing-sources="application.js">Your browser does not support the application. Please use another browser or device.</canvas></div>

    </body>

</html>